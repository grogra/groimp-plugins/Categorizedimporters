# CategorizedImporters 

A simple extension of the PointCloud Importer to add the management of the 'categories' produced by the plugin 'categorizedExports'. It adds the attributes 'catIds' as int[], and 'objectId'.

It includes support of the file type :
- ".ply" 


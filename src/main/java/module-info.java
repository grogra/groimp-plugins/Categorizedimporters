module categorizedImporters {
	exports de.grogra.categorizedImporter;

	requires xl.core;
	requires graph;
	requires pointcloud.importer;
	requires utilities;
}
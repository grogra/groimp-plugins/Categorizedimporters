package de.grogra.categorizedImporter;

import de.grogra.pointcloud.objects.impl.PointCloudMeshImpl;
import de.grogra.util.StringMap;

public class CategorizedMesh extends PointCloudMeshImpl{

	protected int[] catIds;
	//enh:field getter setter
	
	protected int objectId;
	//enh:field getter setter
	
	public CategorizedMesh() {
		super();
	}
	
	public CategorizedMesh(StringMap args) {
		super(args);
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field catIds$FIELD;
	public static final NType.Field objectId$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (CategorizedMesh.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		public void setInt (Object o, int value)
		{
			switch (id)
			{
				case 1:
					((CategorizedMesh) o).objectId = (int) value;
					return;
			}
			super.setInt (o, value);
		}

		@Override
		public int getInt (Object o)
		{
			switch (id)
			{
				case 1:
					return ((CategorizedMesh) o).getObjectId ();
			}
			return super.getInt (o);
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((CategorizedMesh) o).catIds = (int[]) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((CategorizedMesh) o).getCatIds ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new CategorizedMesh ());
		$TYPE.addManagedField (catIds$FIELD = new _Field ("catIds", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (int[].class), null, 0));
		$TYPE.addManagedField (objectId$FIELD = new _Field ("objectId", _Field.PROTECTED  | _Field.SCO, de.grogra.reflect.Type.INT, null, 1));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new CategorizedMesh ();
	}

	public int getObjectId ()
	{
		return objectId;
	}

	public void setObjectId (int value)
	{
		this.objectId = (int) value;
	}

	public int[] getCatIds ()
	{
		return catIds;
	}

	public void setCatIds (int[] value)
	{
		catIds$FIELD.setObject (this, value);
	}

//enh:end
}
